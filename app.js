function distance(first, second) {
	if (!Array.isArray(first) || !Array.isArray(second)) throw new Error('InvalidType');

	if (!first.length || !second.length) return 0;

	// remove duplicates
	const set1 = new Set(first);
	const set2 = new Set(second);

	// remove common elements
	set1.forEach(element => {
		if(set2.has(element)){
			set2.delete(element);
			set1.delete(element);
		}
	});

	return set1.size + set2.size;
}

module.exports.distance = distance